$(document).ready(function() {
  /**
   * Slider Primary
   */
  function doAnimations(elements) {
    var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    elements.each(function() {
      var $this = $(this);
      var $animationDelay = $this.data('delay');
      var $animationType = 'animated ' + $this.data('animation');
      $this.css({
        'animation-delay': $animationDelay,
        '-webkit-animation-delay': $animationDelay
      });
      $this.addClass($animationType).one(animationEndEvents, function() {
        $this.removeClass($animationType);
      });
    });
  }
  /**
   * Get Focus Search 
   */
  $(function() {
    var input_search = $('#search-input');
    input_search.focus(function() {
      $(this).parent().parent().addClass('focus');
    }).blur(function() {
      $(this).parent().parent().removeClass('focus');
    });
  });
  /**
   * Slide Primary
   */
  $(function() {
    var slider = $('#slider-primary .slider');
    var slider_thumb = $('#slider-primary .slider-thumb');
    /**
     * Add event
     */
    slider.on('init', function(e, slick) {
      var $firstAnimatingElements = $('div.slider-item:first-child').find('[data-animation]');
      doAnimations($firstAnimatingElements);    
    });
    slider.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
      var $animatingElements = $('div.slider-item[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
      doAnimations($animatingElements);    
    });
  /**
   * Option Settings
   */
    slider.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-thumb'
    });
    slider_thumb.slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.slider',
      dots: false,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 700,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false
          }
        }
      ]
    });
  });
  /**
   * Waves Effect
   */
  (function() {
    Waves.attach('.btn-waves', ['waves-block', 'waves-light']);
    Waves.attach('.btn-waves-dark', ['waves-block']);
    Waves.init();
  })();
  /**
   * Tabs Animated
   */
  $(function() {
    /**
     * Tabs 1
     */
    $("#myTab a").on("click", function() {

      var position = $(this).parent().position();
      var width = $(this).parent().width();
    
      $("#myTab .floor").css({
        "left": position.left, 
        "width": width
      });
    });
    /**
     * Initialize First
     */
    var actWidth = $("#myTab").find(".active").parent("li").width();
    var actPosition = $("#myTab a.active").position();

    $(".floor").css({
      "left": actPosition.left,
      "width": actWidth
    });
    /**
     * Tabs 2
     */
    $("#myTab2 a").on("click", function() {

      var position = $(this).parent().position();
      var width = $(this).parent().width();
    
      $("#myTab2 .floor").css({
        "left": position.left, 
        "width": width
      });
    });
    /**
     * Initialize First
     */
    var actWidth = $("#myTab2").find(".active").parent("li").width();
    var actPosition = $("#myTab2 a.active").position();

    $("#myTab2 .floor").css({
      "left": actPosition.left,
      "width": actWidth
    });
  });
  /**
   * Menu Responsive
   */
  $(function() {
    var menu_container = $('.top-header .top-wrapper'),
        btn_toggle = $('.top-header .toggle-mobile'),
        search_container = $('.top-header .search-mobile-wrapper'),
        search_toggle = $('.top-header .toggle-search');
    
    btn_toggle.on('click', function() {
      $(this).toggleClass("active");
      if($(this).hasClass('active')) {
        search_toggle.removeClass('active');
        search_container.slideUp();
      }
      menu_container.slideToggle();
    });
    /**
     * Show me Child
     */
    var has_child = menu_container.find('li.has-child');
    has_child.each(function() {
      $(this).on('click', function() {
        $(this).find('ul').slideToggle();
        $(this).siblings().find('ul').slideUp();
      });
    });
    /**
     * Search Toggle
     */
    search_toggle.on('click', function() {
      $(this).toggleClass("active");
      if($(this).hasClass('active')) {
        btn_toggle.removeClass('active');
        menu_container.slideUp();
      }
      search_container.slideToggle();
    });
  });
});