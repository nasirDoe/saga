$(document).ready(function() {
  /**
   * Slider Primary
   */
  $(function() {
    var slider = $('.slider');
    slider.on('initialize.owl.carousel', function() {
      var $firstAnimatingElements = $('div.slider-item:first-child').find('[data-animation]');
      doAnimations($firstAnimatingElements);    
    });
    slider.on('change.owl.carousel', function(e) {
      console.log(e);
      if(e.item.index) {
        var $animatingElements = $('div.slider-item').find('[data-animation]');
        doAnimations($animatingElements);   
      }
    });

    slider.owlCarousel({
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      items: 1,
      loop: false,
      nav: true,
      navText: ['<i class="bx bx-left-arrow-alt"></i>', '<i class="bx bx-right-arrow-alt"></i>'],
      dots: true,
      autoplay: true,
      autoplayTimeout: 6000,
      responsive: {
        0: {
          dots: false,
          nav: false
        },
        768: {
          nav: true,
          dots: true
        }
      }
    });

    function doAnimations(elements) {
      var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
      elements.each(function() {
        var $this = $(this);
        var $animationDelay = $this.data('delay');
        var $animationType = 'animated ' + $this.data('animation');
        $this.css({
          'animation-delay': $animationDelay,
          '-webkit-animation-delay': $animationDelay
        });
        $this.addClass($animationType).one(animationEndEvents, function() {
          $this.removeClass($animationType);
        });
      });
    } 
  });
  /**
   * AOS
   */
  var maxWidth = 1080;
  AOS.init({
    easing: 'ease-out-back',
    duration: 1000,
    disable: function() {
      return window.innerWidth < maxWidth
    }
  });
  /**
   * Toggle Eng
   */
  $(function() {
    var toggle_drop = $('#toggle-eng .drop-eng');
    $('#toggle-eng').on('click', function(e) {
      e.stopPropagation();
      toggle_drop.toggleClass('show');
    });
    $(document).on('click', function() {
      toggle_drop.removeClass('show');
    });
  });
  $(function() {
    $('.btn-showmenu-mobile').on('click', function() {
      $('.menu-primary').toggleClass('show');
    });
    $(document).on('click', '.menu-primary', function() {
      $(this).toggleClass('show');
    });
    $('.menu').on('click', function(e) {
      e.stopPropagation();
    });
  });
});